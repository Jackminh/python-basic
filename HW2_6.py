#create numpy array from a list
import numpy as np
x = range(1,11)
a1 = np.array(x,'i')
a2 = np.array(x,'f')
print (a1.dtype)
print (a2.dtype)

#create arrays in different ways
arr1 = np.zeros((2,3,4))
arr2 = np.ones((2,3,4))
arr3 = np.arange(1000)

#indexing and slicing arrays
a = np.array([2.0,3.2,5.5,-6.4,-2.2,2.4])
print(a[1])
print(a[1:4])
a = np.array([[2,3.2,5.5,-6.4,-2.2,2.4],
             [1,22,4,0.1,5.3,-9],
             [3,1,2.1,21,1.1,-2]])
print(a)
print(a[:,3])
print(a[1:4,0:4])
print(a[1:,2])

#interrogate an array
arr=np.array([range(4),range(10,14)])

print(arr.shape)
print(arr.size)
print(arr.max())
print(arr.min())

#generate new arrays by modifying array
print(np.reshape(arr, (2,2,2)))
print(np.transpose(arr))
print(np.ravel(arr))
print(arr.astype('f'))

#perform array calculation
a = np.array([range(4),range(10,14)])
print(np.reshape(a,(2,4)))
b = np.array([2,-1,1,0])
print(a*b)
b1=b*100
b2=b*100.0
print(b1)
print(b2)
print(b1==b2)
print(b1.dtype)
print(b2.dtype)

#Array comparisions

arr=np.arange(10)
print(arr < 3)
print(np.less(arr,3))
condition = np.logical_or(arr<3, arr>8)
new_arr = np.where(condition,arr*5, arr*-5)
print(new_arr)

#math function that works on array

def magnitude(u,v,minmag=0.1):
    mag = ((u**2)+(v**2))**0.5
    output=np.where(mag>minmag,mag,minmag)
    return output
u=np.array([[4,5,6],[2,3,4]])
v=np.array([[2,2,2],[1,1,1]])
print(magnitude(u,v))

#create masked array
import numpy.ma as MA
marr=MA.masked_array(range(10),fill_value=-999)
print(marr, marr.fill_value)
marr[2]=MA.masked
print(marr)
print(marr.mask)
narr=MA.masked_where(marr>7,marr)
print(narr)
print(MA.filled(narr))
print(type(x))

#create mask smaller than overall way
m1 = MA.masked_array(range(1,9))
print(m1)
m2 = m1.reshape(2,4)
print(m2)
m3=MA.masked_where(m2>6,m2)
print(m3)
print(m3*100)
res=m3-np.ones((2,4))
print(res)
print(type(res))

