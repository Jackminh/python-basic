from datetime import datetime

start_date = datetime.strptime(input('Enter start date in the format m/d/y:  '), '%m/%d/%Y')
end_date = datetime.strptime(input('Enter end date in the format m/d/y:  '), '%m/%d/%Y')
delta = end_date - start_date
print("Number of days is: " + str(delta.days) + " days")