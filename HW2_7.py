N = int(input("enter N = "))
import random
import numpy as np
random_list =[]
for x in range(N**3):
    random_list.append(random.uniform(0,10))
arr= np.reshape(np.array(random_list),(N,N,N))
print(arr)

arr_x= np.sum(arr,axis=0)
min_arr_x_index=np.argmin([arr_x] )
max_arr_x_index=np.argmax([arr_x])
print("sum value in axis 0 is: " + str(arr_x))
print("min sum value array in axis 0 is: " + str(arr_x.min()) +" at index " + str(min_arr_x_index))
print("max sum value array in axis 0 is: " + str(arr_x.max()) + " at index " + str(max_arr_x_index))

arr_y= np.sum(arr,axis=1)
min_arr_y_index=np.argmin([arr_y] )
max_arr_y_index=np.argmax([arr_y])
print("sum value in axis 1 is: " + str(arr_y))
print("min sum value array in axis 1 is: " + str(arr_y.min()) +" at index " + str(min_arr_y_index))
print("max sum value array in axis 1 is: " + str(arr_y.max()) + " at index " + str(max_arr_y_index))

arr_z= np.sum(arr,axis=2)
min_arr_z_index=np.argmin([arr_z] )
max_arr_z_index=np.argmax([arr_z])
print("sum value in axis 2 is: " + str(arr_z))
print("min sum value array in axis 2 is: " + str(arr_z.min()) +" at index " + str(min_arr_z_index))
print("max sum value array in axis 2 is: " + str(arr_z.max()) + " at index " + str(max_arr_z_index))