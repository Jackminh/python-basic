#Create and print a histogram of the list
n = int(input("Number of samples: "))
random_list =[]
import random
for x in range(n):
    random_list.append(random.randint(0,10))
print("random intergers are: " + str(random_list))

def histogram( items ):
    for i in items:
        output = ''
        while( i > 0 ):
          output += '*'
          i = i - 1
        print(output)

histogram(random_list)

#Calculate and print the statistics description of the list
import statistics as st

mean_list = st.mean(random_list)
variance_list = st.variance(random_list)
stdev_list = st.stdev(random_list)

print ("max in random list is: " + str(max(random_list)))
print ("min in random list is: " + str(min(random_list)))
print ("mean in random list is: " + str(mean_list))
print ("variance in random list is: " + str(variance_list))
print ("standard deviation in random list is: " + str(stdev_list))

#Remove duplicates from the list
print("List of distinct items: " + str(set(random_list)))