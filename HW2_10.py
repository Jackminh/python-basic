# what is the future value after 10 years of saving $1000 now,
# additional monthly savings of $1000.
# interest rate is 5% (annually)
import numpy as np
print(np.fv(0.05/12, 10*12, -1000, -1000))
a = np.array((0.05, 0.06, 0.07))/12
print(np.fv(a, 10*12, -1000, -1000))

# What is the present value of an investment that needs to total $100000
# after 10 years of saving $1000 every month?
# interest rate is 5% (annually) compounded monthly.

print(np.pv(0.05/12, 10*12, -1000, 100000))

#Net Present Value
#discount rate 0.281

print(np.npv(0.281,[-100, 39, 59, 55, 20]))

#What is the monthly payment needed to pay off a $200,000 loan
# in 15 years at an annual interest rate of 7.5%?

print(np.pmt(0.075/12, 12*15, 200000))

#IRR
print(round(irr([-100, 39, 59, 55, 20]), 5))

#If you only had $150/month to pay towards the loan,
# how long would it take to pay-off a loan of $8,000 at 7% annual interest?

print(round(np.nper(0.07/12, -150, 8000), 5))

