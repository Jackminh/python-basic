def input_number(message):
  while True:
    try:
       user_input = int(input(message))
    except ValueError:
       print("Not an integer! Try again.")
       continue
    else:
        if user_input <=0:
            print("Not an positive integer! Try again.")
            continue
        else:
            return user_input
            break

n = input_number("enter amount of numbers: ")

list_input = []
list_odd = []
list_even = []

for i in range(0, n):
    number = input_number('Please enter a number: ')
    list_input.append(number)
    if list_input[i]%2==0:
        list_even.append(list_input[i])
    else:
        list_odd.append(list_input[i])

print("list of number is: " + str(list_input))
print("list of odd number is: " + str(list_odd))
print("list of even number is: " + str(list_even))